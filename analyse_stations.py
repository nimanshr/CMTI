import math
import os.path as op
import numpy as num
from matplotlib import pyplot as plt, cm, colors, gridspec

from pyrocko import model, pile, gf, trace, util
from pyrocko.fdsn import station as fs
from pyrocko.guts import Object, List, Tuple, Float, Int, String, load
from pyrocko.guts_array import Array
from pyrocko.plot import mpl_init, mpl_margins, mpl_papersize

util.setup_logging('analyse_stations.py', 'warning')


class SpectraCollection(Object):
    codes_list = List.T(Tuple.T(4, String.T()))
    event_name_list = List.T(String.T())
    frequency_delta = Float.T()
    nfrequencies = Int.T()
    nspectra = Int.T(default=0)
    index = Array.T(optional=True, dtype=num.int, shape=(None, 2))

    def __init__(self, **kwargs):
        Object.__init__(self, **kwargs)
        self._event_name_index = dict(
            (event_name, i) for (i, event_name)
            in enumerate(self.event_name_list))

        self.codes_list = map(tuple, self.codes_list)

        self._codes_index = dict(
            (codes, i) for (i, codes)
            in enumerate(self.codes_list))

        self._data_fn = None
        self._data = None
        self._index_buffer = None

    def get_frequencies(self):
        return num.arange(self.nfrequencies) * self.frequency_delta

    def set_data_filename(self, fn, truncate=False):
        self._data_fn = fn
        if truncate:
            with open(self._data_fn, 'wb'):
                pass

    def _write_data(self, spectrum):
        with open(self._data_fn, 'ab') as f:
            spectrum.astype('<c16').tofile(f)

    def add(self, event_name, codes, spectrum):
        assert spectrum.shape == (self.nfrequencies,)

        if event_name not in self._event_name_index:
            self.event_name_list.append(event_name)
            self._event_name_index[event_name] = len(self.event_name_list) - 1

        if codes not in self._codes_index:
            self.codes_list.append(codes)
            self._codes_index[codes] = len(self.codes_list) - 1

        ievent = self._event_name_index[event_name]
        icodes = self._codes_index[codes]

        self._write_data(spectrum)
        if self.index is None:
            self._index_buffer = num.zeros((2, 2), dtype=num.int)
            self.index = self._index_buffer[:0, :]

        if self.nspectra + 1 > self._index_buffer.shape[0]:
            temp = num.zeros(
                (self._index_buffer.shape[0] * 2, 2), dtype=num.int)

            temp[:self.nspectra, :] = self._index_buffer[:, :]
            self._index_buffer = temp
            self.index = self._index_buffer[:self.nspectra]
            print self.nspectra

        self.index = self._index_buffer[:self.nspectra+1, :]
        self.index[self.nspectra, :] = (ievent, icodes)
        self.nspectra += 1

    def get(self, event_name=None, codes_selector=None):
        if self._data is None:
            self._data = num.memmap(
                self._data_fn,
                dtype='<c16',
                mode='r',
                shape=(self.nspectra, self.nfrequencies))

        if codes_selector:
            icodes = set(
                i for (i, codes)
                in enumerate(self.codes_list) if codes_selector(codes))

            codes_mask = num.zeros(self.index.shape[0], dtype=num.bool)
            for ii in xrange(self.index.shape[0]):
                codes_mask[ii] = self.index[ii, 1] in icodes
        else:
            codes_mask = True

        if event_name is not None:
            ievent = self._event_name_index[event_name]
            event_mask = self.index[:, 0] == ievent
        else:
            event_mask = True

        if event_mask is True and codes_mask is True:
            return self._data

        return self._data[num.logical_and(codes_mask, event_mask), :]


sites = ('geofon', 'iris')
dn_data = 'data'
global_store_id = 'global_2s'
t_pad = 10*60.

tinc_windowing = 100.
tpad_windowing = 50.


fn_events_selected = 'catalogs/events-selected-gcmt.txt'
fn_response_template = op.join(
    'meta', 'responses', '%(site)s',
    'response-%(network)s.%(station)s.%(location)s.%(channel)s.xml')

dn_event_template = op.join(dn_data, '%(event_name)s')

dn_raw_template = op.join(dn_event_template, 'observed', '%(site)s')
fn_raw_template = op.join(
    dn_raw_template,
    '%(network)s.%(station)s.%(location)s.%(channel)s.%(tmin)s.mseed')

fn_spectra_collection = 'spectra/noise'

util.ensuredirs(fn_spectra_collection)

engine = gf.LocalEngine(
    store_superdirs=['/home/nooshiri/gf_stores'],
    use_config=True)

events_selection = model.load_events(fn_events_selected)
global_store = engine.get_store(global_store_id)

target_sample_rate = global_store.config.sample_rate

sxs_resp = {}
network_to_color = {}

collection = None

for iev, ev in list(enumerate(events_selection))[:]:

    print '-'*100
    print iev, ev.name

    piles = {}
    for site in sites:
        dn_raw = dn_raw_template % dict(site=site, event_name=ev.name)

        piles[site] = pile.make_pile(dn_raw, fileformat='detect')

    for site in sites:
        for nslc in piles[site].nslc_ids.keys():
            if (site, nslc) not in sxs_resp:
                net, sta, loc, cha = nslc
                fn_resp = fn_response_template % dict(
                    site=site,
                    network=net,
                    station=sta,
                    location=loc,
                    channel=cha)

                sxs_resp[site, nslc] = fs.load_xml(
                    filename=fn_resp)

            sx_resp = sxs_resp[site, nslc]

            stations = sx_resp.get_pyrocko_stations(
                nslcs=[nslc], time=ev.time)

            if len(stations) != 1:
                continue

            station = stations[0]

            resp = sx_resp.get_pyrocko_response(
                nslc,
                time=ev.time,
                fake_input_units='M/S')

            source = gf.MTSource.from_pyrocko_event(ev)

            t_p = ev.time + global_store.t(
                '{stored:P}', source, station)

            t_s = ev.time + global_store.t(
                '{stored:S}', source, station)

            if None in (t_p, t_s):
                continue

            t_len = t_s - t_p + t_pad
            tmin_selection = t_p - t_len
            tmax_selection = t_p + t_len

            tpad_proc = 20. / target_sample_rate

            nsamp_want = int(round(
                (tinc_windowing+2*tpad_windowing) * target_sample_rate))

            for trs_raw in piles[site].chopper(
                    tmin=tmin_selection + tpad_windowing + tpad_proc,
                    tmax=t_p - tpad_windowing - tpad_proc,
                    tinc=tinc_windowing,
                    tpad=tpad_windowing + tpad_proc,
                    trace_selector=lambda tr_raw: tr_raw.nslc_id == nslc,
                    want_incomplete=False):

                if len(trs_raw) == 1:

                    tr_raw = trs_raw[0]

                    try:
                        tr_raw.downsample_to(1./target_sample_rate, snap=True)
                        tr_raw.chop(
                            tr_raw.wmin - tpad_windowing,
                            tr_raw.wmax + tpad_windowing)

                        if tr_raw.data_len() != nsamp_want:
                            continue

                        tr_raw.ydata -= num.mean(tr_raw.ydata)

                        tr_raw.ydata *= num.hanning(tr_raw.data_len())

                        freqs, coeffs = tr_raw.spectrum(pad_to_pow2=True)

                        coeffs[0] = 0.0
                        coeffs[1:] = coeffs[1:] / resp.evaluate(freqs[1:])

                        if collection is None:
                            collection = SpectraCollection(
                                frequency_delta=float(freqs[1]),
                                nfrequencies=coeffs.size)

                            collection.set_data_filename(
                                fn_spectra_collection + '.data',
                                truncate=True)

                        collection.add(ev.name, nslc, coeffs)

                    except (fs.NoResponseInformation,
                            util.UnavailableDecimation,
                            trace.NoData):
                        print '-- NoK --'
                        pass


    collection.dump(filename=fn_spectra_collection + '.info')

collection = load(filename=fn_spectra_collection + '.info')

collection.set_data_filename(fn_spectra_collection + '.data')

networks = sorted(set(nslc[0] for nslc in collection.codes_list))
nsls = sorted(set(nslc[:3] for nslc in collection.codes_list))

trusted_networks = ('II', 'IU', 'G', 'GE')

comps = ['vertical', 'horizontal']


def comp_selector(comp, nslc):
    return (comp == 'vertical' and nslc[-1].endswith('Z')) \
        or (comp == 'horizontal' and not nslc[-1].endswith('Z'))


median_log_background = {}
for comp in comps:
    background = collection.get(
        codes_selector=lambda nslc: nslc[0] in trusted_networks
        and comp_selector(comp, nslc))

    print background.shape
    ii_nonzero = num.where(num.any(background[:] != 0.0, axis=1))[0]
    background = background[ii_nonzero]
    log_background = num.zeros_like(background, dtype=num.float)
    log_background[:, 1:] = num.log(num.abs(background[:, 1:]))

    median_log_background[comp] = num.median(log_background, axis=0)


fmin = 0.01
fmax = 0.1

frequencies = collection.get_frequencies()

ii_fband = num.logical_and(fmin <= frequencies, frequencies <= fmax)

f = open('station_badnesses.txt', 'w')

weights = num.array([1.0, 1.0])
weights /= num.sum(weights)

mappable = cm.ScalarMappable(
    norm=colors.Normalize(0., 7.),
    cmap=plt.get_cmap('rainbow'))

mappable.set_array(num.zeros(0))

fontsize = 9.
mpl_init(fontsize=fontsize)

for network in networks:
    fig = plt.figure(figsize=mpl_papersize('a4', 'portrait'))
    labelpos = mpl_margins(fig, w=7., h=5., units=fontsize)

    gs = gridspec.GridSpec(3, 1, height_ratios=[10, 10, 1])

    axes = {}
    axes['vertical'] = fig.add_subplot(gs[0])
    axes['horizontal'] = fig.add_subplot(gs[1])
    axes['bar'] = fig.add_subplot(gs[2])
    labelpos(axes['vertical'], 2., 1.5)
    labelpos(axes['horizontal'], 2., 1.5)

    axes['vertical'].set_ylim(-9., -5.)
    axes['horizontal'].set_ylim(-9., -5.)

    for nsl in nsls:

        if nsl[0] != network:
            continue

        misfits = []
        median_log_spectra = []
        for comp in comps:
            spectra = collection.get(
                codes_selector=lambda nslc: nslc[:3] == nsl
                and comp_selector(comp, nslc))

            ii_nonzero = num.where(num.any(spectra[:] != 0.0, axis=1))[0]

            if ii_nonzero.size == 0:
                continue

            spectra = spectra[ii_nonzero]

            log_spectra = num.zeros_like(spectra, dtype=num.float)
            log_spectra[:, 1:] = num.log(num.abs(spectra[:, 1:]))

            median_log_spectrum = num.median(log_spectra, axis=0)

            misfits.append(
                num.sqrt(num.sum(
                    (median_log_spectrum[ii_fband]
                     - median_log_background['vertical'][ii_fband])**2)))

            median_log_spectra.append(median_log_spectrum)

        if len(misfits) != 2:
            continue

        misfit = math.sqrt(num.sum(num.array(weights*misfits)**2))
        print '.'.join(nsl), misfit
        f.write('%s %g\n' % ('.'.join(nsl), misfit))

        for comp, median_log_spectrum in zip(comps, median_log_spectra):
            axes[comp].plot(
                frequencies[1:],
                median_log_spectrum[1:]/num.log(10.),
                lw=0.5,
                label='.'.join(nsl),
                color=mappable.to_rgba(misfit))

    for comp in comps:
        if comp != 'vertical':
            axes[comp].plot(
                frequencies[1:],
                median_log_background[comp][1:]/num.log(10.),
                '--',
                lw=2,
                color='black')

        axes[comp].plot(
            frequencies[1:],
            median_log_background['vertical'][1:]/num.log(10.),
            lw=2,
            color='black')

        axes[comp].set_xscale('log')
        axes[comp].set_xlim(frequencies[1], frequencies[-1])
        axes[comp].set_xlabel('Frequency [Hz]')

    cbar = fig.colorbar(
        mappable,
        orientation='horizontal',
        cax=axes['bar'],
        ticks=[0., 1., 2., 3., 5., 6., 7.])

    cbar.set_label('Badness')

    # axes['horizontal'].legend(loc='lower left')

    fig.suptitle(network)

    fig.savefig('plots/spectra/spectra_%s.pdf' % network)

f.close()
