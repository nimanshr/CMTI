from collections import defaultdict
import os.path as op

import matplotlib.pyplot as plt

import grond
from pyrocko import guts, model, moment_tensor


dn_gruns_init = 'gruns.run1'
dn_gruns_final = 'gruns.final'

fn_rundir_template = op.join('%(dn_gruns)s', 'bodywave_%(event_name)s.run')

fn_events_selected = 'catalogs/events-selected-gcmt.txt'
fn_events_notused = 'catalogs/events-notused.txt'


events_selection = model.load_events(fn_events_selected)
events_notused = open(fn_events_notused, 'r').read().splitlines()

events = [e for e in events_selection if e.name not in events_notused]


class GrondResult(object):
    def __init__(self, rundir):
        self.rundir = rundir
        self.config = guts.load(filename=op.join(rundir, 'config.yaml'))
        self.config.set_basepath(rundir)

        problem, xs, misfits = grond.load_problem_info_and_data(
            rundir,
            subset='harvest')
        self.problem = problem
        self.xs = xs
        self.misfits = misfits

        self.best_source = grond.core.get_best_source(
            self.problem,
            self.xs,
            self.misfits)

        self.mean_source = grond.core.get_mean_source(self.problem, self.xs)


events_out_init = []
events_out_final = []
for event in events[:]:

    print event.name

    fn_rundir_init = fn_rundir_template % dict(
        dn_gruns=dn_gruns_init,
        event_name=event.name)

    r_init = GrondResult(fn_rundir_init)
    src_init = r_init.best_source
    events_out_init.append(src_init.pyrocko_event())

    fn_rundir_final = fn_rundir_template % dict(
        dn_gruns=dn_gruns_final,
        event_name=event.name)

    r_final = GrondResult(fn_rundir_final)
    src_final = r_final.best_source
    events_out_final.append(src_final.pyrocko_event())


guts.dump_all(events_out_init, filename='events-out-%s-init.txt' % 'best')
guts.dump_all(events_out_final, filename='events-out-%s-final.txt' % 'best')
