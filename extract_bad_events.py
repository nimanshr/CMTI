import glob

import grond


rundirs = glob.glob('data/gruns/bodywave_201*A.run')

gm_max = 0.7

bad_events = []
for rundir in rundirs:
    problem, xs, misfits = grond.load_problem_info_and_data(rundir)
    gms = problem.global_misfits(misfits)

    print problem.base_source.name, min(gms)

    if min(gms) > gm_max:
        bad_events.append((problem.base_source.name, min(gms)))

with open('bad_events.txt', 'w') as f:
    for event, gm in bad_events:
        line = "%s  %8.4f\n" % (event, gm)
        f.write(line)
